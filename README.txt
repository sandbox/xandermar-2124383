Requires the use of the Summary field that precedes the Body field in a 
standard content type. It, by default, also expands the summary field and 
makes it mandatory.

(Drupal 7) If using the Body field, you MUST select "Summary input" on the 
Body field's settings page:

admin/structure/types/manage/[content type name]/fields/body

No configuration needed - simply install the module and enable.

Installation via Drush:

1. Go to the root path of your site
2. To download, type: drush dl mandatory_summary
3. To enable, type: drush en mandatory_summary
4. If multisite, type: drush en mandatory_summary -l [site folder name]

Create content using Basic page, Article or whatever uses a Body field or 
create a new content type.
